# uranosqgistools

Toolkit for converting QGIS XYZ outputs to URANOS simulation files.

This work was supported by the Royal Commission for the Exhibition of 1851.